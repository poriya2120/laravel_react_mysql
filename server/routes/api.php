<?php

use Illuminate\Http\Request;
// use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Route; 

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


// Route::get('tasks','TaskController@index');
// Route::get('tasks/{id}','TaskController@show');
// // Route::post('tasks','TaskController@store');
// // Route::post('tasks', function () {
// //     return 'Hello World';
// // });
// Route::post('tasks}', function () {
//    return 'hello';
// });
// Route::put('tasks/{id}','TaskController@update');
// Route::delete('tasks/{id}','TaskController@delete');

// Route::get('tasks', 'TaskController@index');
// Route::get('tasks/{id}', 'TaskController@show');
// Route::post('tasks', 'TaskController@store');
// Route::put('tasks/{id}', 'TaskController@update');
// Route::delete('tasks/{id}', 'TaskController@delete');

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
// oute::group(['middleware' => 'auth:api'], function() {R
    Route::get('tasks', 'TaskController@index');
    Route::get('tasks/{id}', 'TaskController@show');
    Route::post('tasks', 'TaskController@store');
    Route::put('tasks/{id}', 'TaskController@update');
    Route::delete('tasks/{id}', 'TaskController@delete');

// });
